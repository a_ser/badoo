package com.test.badoochallengetask.base.presenter;


public interface IPresenter {

    void onDestroyView();
}
