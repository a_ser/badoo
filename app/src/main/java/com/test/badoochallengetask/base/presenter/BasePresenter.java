package com.test.badoochallengetask.base.presenter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.test.badoochallengetask.base.view.IView;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


public abstract class BasePresenter<V extends IView> implements IPresenter {
    private final CompositeDisposable mDisposables = new CompositeDisposable();
    @Nullable
    private V mView;

    public BasePresenter(@NonNull final V view) {
        mView = view;
    }

    protected void addToDisposable(final Disposable disposable) {
        mDisposables.add(disposable);
    }

    @Override
    public void onDestroyView() {
        mDisposables.clear();
        mView = null;
    }

    @NonNull
    protected V getView() {
        if (mView == null) {
            throw new IllegalStateException("You can't interact with view before or after it attached/detached from view");
        }
        return mView;
    }

    protected void publishState(@NonNull final RequestState state) {
        final Disposable subscribe = Observable.just(state)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(requestState -> {
                    switch (requestState) {
                        case LOADING:
                            getView().manageLoadingVisibility(true);
                            break;
                        default:
                            getView().manageLoadingVisibility(false);
                    }
                });
        addToDisposable(subscribe);
    }

    protected enum RequestState {
        LOADING, COMPLETE, ERROR
    }
}
