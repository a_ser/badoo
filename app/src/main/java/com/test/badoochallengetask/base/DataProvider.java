package com.test.badoochallengetask.base;

import android.content.Context;
import android.content.res.AssetManager;
import android.support.annotation.NonNull;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by andrii on 20/05/2017.
 */

public class DataProvider {
    @NonNull
    private final Context mContext;

    public DataProvider(@NonNull final Context context) {
        mContext = context;
    }

    public InputStream openAssets(@NonNull String fileName) throws IOException {
        AssetManager am = mContext.getAssets();
        return am.open(fileName);
    }
}
