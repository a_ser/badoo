package com.test.badoochallengetask.base.view;

public interface IView {
    void manageLoadingVisibility(boolean isVisible);
}
