package com.test.badoochallengetask.base.view;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import com.test.badoochallengetask.base.presenter.IPresenter;

import javax.inject.Inject;

public abstract class BaseFragment<P extends IPresenter> extends Fragment implements IView, Router {
    @Nullable
    private P mPresenter;
    @Nullable
    private String mTitle;

    @Override
    @CallSuper
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mPresenter == null) {
            throw new IllegalStateException("You should initialise presenter");
        }
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        setTitle(getTitle() != null ? getTitle() : "");

    }

    public void setTitle(@NonNull final String title) {
        if (mTitle == null) {
            mTitle = title;
        }
        getActivity().setTitle(mTitle);
    }

    @Nullable
    protected String getTitle() {
        return null;
    }

    @NonNull
    protected P getPresenter() {
        if (mPresenter == null) {
            throw new IllegalStateException("Presenter should be inject before using it");
        }
        return mPresenter;
    }

    @Inject
    protected void setPresenter(@NonNull final P presenter) {
        mPresenter = presenter;
    }

    @Override
    public void navigateToFragment(@NonNull final Fragment fragment) {
        ((Router) getActivity()).navigateToFragment(fragment);
    }

    @Override
    public void onDestroyView() {
        getPresenter().onDestroyView();
        super.onDestroyView();
    }
}
