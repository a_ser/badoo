package com.test.badoochallengetask.base.view;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

public interface Router {

    void navigateToFragment(@NonNull Fragment fragment);
}
