package com.test.badoochallengetask.screens.products;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.badoochallengetask.R;
import com.test.badoochallengetask.models.Product;
import com.test.badoochallengetask.screens.base_list.BaseListAdapter;
import com.test.badoochallengetask.utils.Action1;

import java.util.List;

class ProductsAdapter extends BaseListAdapter<List<Product>> {
    @NonNull
    private final Action1<Product> mProductClickAction;

    ProductsAdapter(@NonNull final Action1<Product> productClickAction) {
        mProductClickAction = productClickAction;
    }


    @Override
    public ListViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_product, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder originalHolder, final int position) {
        ListViewHolder holder = (ListViewHolder) originalHolder;
        final Product product = getDataOrThrow().get(position);
        final Context context = holder.itemView.getContext();
        holder.productNameView.setText(product.getSku());
        holder.transactionsView.setText(context.getString(R.string.transactions_xd, product.getTransactions().size()));
        holder.itemView.setOnClickListener(view -> mProductClickAction.call(product));
    }

    @Override
    public int getItemCount() {
        return getData() == null ? 0 : getData().size();
    }

    public void clear() {
        if (getData() != null) {
            getData().clear();
            notifyDataSetChanged();
        }
    }

    private static class ListViewHolder extends RecyclerView.ViewHolder {
        final TextView productNameView;
        final TextView transactionsView;

        ListViewHolder(final View itemView) {
            super(itemView);
            productNameView = (TextView) itemView.findViewById(R.id.view_holder_product_name);
            transactionsView = (TextView) itemView.findViewById(R.id.view_holder_transactions);
        }
    }
}
