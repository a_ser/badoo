package com.test.badoochallengetask.screens.products;

import android.support.annotation.NonNull;

import com.test.badoochallengetask.base.presenter.BasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

class ProductsPresenter extends BasePresenter<ProductsFragment> {

    @Inject
    ProductsInteractor mInteractor;

    @Inject
    ProductsPresenter(@NonNull final ProductsFragment view) {
        super(view);
    }

    void loadData() {
        final Disposable disposable = mInteractor
                .loadList()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(d -> publishState(RequestState.LOADING))
                .doOnSuccess(d -> publishState(RequestState.COMPLETE))
                .doOnError(d -> publishState(RequestState.ERROR))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((products, throwable) -> {
                    if (throwable != null) {
                        Timber.e(throwable);
                        getView().displayErrorMessage(throwable);
                    } else if (products != null) {
                        getView().updateProduct(products);
                    }
                });
        addToDisposable(disposable);
    }
}
