package com.test.badoochallengetask.screens.transactions;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.test.badoochallengetask.MainApplication;
import com.test.badoochallengetask.R;
import com.test.badoochallengetask.models.Product;
import com.test.badoochallengetask.screens.base_list.BaseListAdapter;
import com.test.badoochallengetask.screens.base_list.BaseListFragment;
import com.test.badoochallengetask.screens.transactions.di.DaggerTransactionsComponent;
import com.test.badoochallengetask.screens.transactions.di.TransactionsComponent;
import com.test.badoochallengetask.screens.transactions.di.TransactionsModule;
import com.test.badoochallengetask.screens.transactions.model.TransactionManager;

public class TransactionsFragment extends BaseListFragment<TransactionPresenter, TransactionManager> {
    private static final String PRODUCT_EXTRA = "product";
    private TransactionsComponent mComponent;

    @NonNull
    public static TransactionsFragment create(@NonNull final Product product) {
        final TransactionsFragment transactionsFragment = new TransactionsFragment();
        final Bundle bundle = new Bundle(1);
        bundle.putParcelable(PRODUCT_EXTRA, product);
        transactionsFragment.setArguments(bundle);
        return transactionsFragment;
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getPresenter().load();
    }

    @Override
    protected void injectComponents() {
        getComponent().inject(this);
    }

    @NonNull
    @Override
    protected BaseListAdapter<TransactionManager> getAdapter() {
        return new TransactionsAdapter();
    }

    @Override
    public void setTitle(@NonNull final String sku) {
        super.setTitle(getContext().getString(R.string.transactions_for_xs, sku));
    }

    @Override
    public void showNoDataAvailable() {
        setTitle(getString(R.string.transactions));
        super.showNoDataAvailable();
    }

    @NonNull
    private TransactionsComponent getComponent() {
        if (mComponent == null) {
            mComponent = DaggerTransactionsComponent.builder()
                    .appComponent(((MainApplication) getContext().getApplicationContext()).getAppComponent())
                    .transactionsModule(new TransactionsModule(this, getArguments().getParcelable(PRODUCT_EXTRA)))
                    .build();
        }
        return mComponent;
    }
}
