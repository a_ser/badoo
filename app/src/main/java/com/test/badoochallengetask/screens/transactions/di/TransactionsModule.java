package com.test.badoochallengetask.screens.transactions.di;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.test.badoochallengetask.models.Product;
import com.test.badoochallengetask.screens.transactions.TransactionsFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class TransactionsModule {
    @NonNull
    private final TransactionsFragment mView;
    @Nullable
    private final Product mProduct;

    public TransactionsModule(@NonNull final TransactionsFragment view, @Nullable final Product product) {
        mView = view;
        mProduct = product;
    }

    @Provides
    @NonNull
    TransactionsFragment providesView() {
        return mView;
    }

    @Provides
    @Nullable
    Product providesProduct() {
        return mProduct;
    }
}
