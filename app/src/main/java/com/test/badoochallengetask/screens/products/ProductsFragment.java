package com.test.badoochallengetask.screens.products;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.test.badoochallengetask.MainApplication;
import com.test.badoochallengetask.R;
import com.test.badoochallengetask.models.Product;
import com.test.badoochallengetask.screens.base_list.BaseListFragment;
import com.test.badoochallengetask.screens.products.di.DaggerProductsComponent;
import com.test.badoochallengetask.screens.products.di.ProductsComponent;
import com.test.badoochallengetask.screens.products.di.ProductsModule;
import com.test.badoochallengetask.screens.transactions.TransactionsFragment;

import java.util.List;


public class ProductsFragment extends BaseListFragment<ProductsPresenter, List<Product>> {

    private ProductsComponent mComponent;

    @NonNull
    public static Fragment create() {
        return new ProductsFragment();
    }

    @Override
    protected void injectComponents() {
        getComponent().inject(this);
    }

    @NonNull
    @Override
    protected ProductsAdapter getAdapter() {
        return new ProductsAdapter(this::onOpenProductItem);
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getPresenter().loadData();
    }

    @Nullable
    @Override
    protected String getTitle() {
        return getString(R.string.fragment_list_title);
    }

    public void updateProduct(@NonNull final List<Product> products) {
        super.updateData(products);
    }

    @NonNull
    private ProductsComponent getComponent() {
        if (mComponent == null) {
            mComponent = DaggerProductsComponent.builder()
                    .appComponent(((MainApplication) getContext().getApplicationContext()).getAppComponent())
                    .productsModule(new ProductsModule(this))
                    .build();
        }
        return mComponent;
    }

    private void onOpenProductItem(@NonNull final Product product) {
        navigateToFragment(TransactionsFragment.create(product));
    }
}
