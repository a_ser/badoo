package com.test.badoochallengetask.screens.transactions.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.test.badoochallengetask.models.ProductPrice;

import java.util.ArrayList;
import java.util.List;

public class TransactionManager {
    @Nullable
    private ProductPrice mTotalPrice;
    @NonNull
    private List<PriceItem> mTransactions;

    public TransactionManager() {
        mTransactions = new ArrayList<>();
    }

    /**
     * Add price to original and converted price to transaction
     *
     * @param priceItem with original price and it's converted representation
     * @throws IllegalStateException when converted price's currency has change
     */
    public void addTransaction(@NonNull final PriceItem priceItem) {
        mTransactions.add(priceItem);
        final ProductPrice convertedPrice = priceItem.getConvertedPrice();
        if (mTotalPrice == null) {
            mTotalPrice = new ProductPrice(convertedPrice.getAmount(), convertedPrice.getCurrency());
        } else {
            if (!mTotalPrice.getCurrency().equals(priceItem.getConvertedPrice().getCurrency())) {
                throw new IllegalStateException("Currency time of converted time cannot be different");
            }
            mTotalPrice.setAmount(mTotalPrice.getAmount().add(convertedPrice.getAmount()));
        }
    }

    @Nullable
    public ProductPrice getTotalPrice() {
        return mTotalPrice;
    }

    @NonNull
    public List<PriceItem> getTransactions() {
        return mTransactions;
    }

    public static class PriceItem {
        @NonNull
        ProductPrice mCurrentPrice;
        @NonNull
        ProductPrice mConvertedPrice;

        public PriceItem(@NonNull final ProductPrice currentPrice, @NonNull final ProductPrice convertedPrice) {
            mCurrentPrice = currentPrice;
            mConvertedPrice = convertedPrice;
        }

        @NonNull
        public ProductPrice getCurrentPrice() {
            return mCurrentPrice;
        }

        @NonNull
        public ProductPrice getConvertedPrice() {
            return mConvertedPrice;
        }
    }
}
