package com.test.badoochallengetask.screens.transactions;

import com.google.gson.Gson;
import com.test.badoochallengetask.base.DataProvider;
import com.test.badoochallengetask.exceptions.IllegalRateFormat;
import com.test.badoochallengetask.models.Rate;
import com.test.badoochallengetask.utils.Constants;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.schedulers.Schedulers;

class TransactionInteractor {
    private static final String RATES_FILE_NAME = "rates.json";
    @Inject
    DataProvider mDataProvider;

    @Inject
    TransactionInteractor() {

    }

    Single<RateManager> getRateManager() {
        return Single.create((SingleOnSubscribe<RateManager>) singleEmitter -> {
            try {
                final InputStream inputStream = mDataProvider.openAssets(RATES_FILE_NAME);
                Reader reader = new InputStreamReader(inputStream, Constants.UTF_8);
                final Rate[] rates = new Gson().fromJson(reader, Rate[].class);
                final RateManager rateManager = new RateManager();
                for (final Rate rate : rates) {
                    if (rate.getFrom() == null || rate.getRate() == null || rate.getTo() == null) {
                        singleEmitter.onError(new IllegalRateFormat("Rate shouldn't have empty from, to and rate"));
                        return;
                    }
                    rateManager.add(rate.getFrom(), rate.getRate(), rate.getTo());
                }
                singleEmitter.onSuccess(rateManager);
            } catch (Exception exception) {
                singleEmitter.onError(exception);
            }
        }).subscribeOn(Schedulers.computation());
    }
}
