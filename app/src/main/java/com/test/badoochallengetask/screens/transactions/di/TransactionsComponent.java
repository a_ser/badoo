package com.test.badoochallengetask.screens.transactions.di;

import com.test.badoochallengetask.di.AppComponent;
import com.test.badoochallengetask.di.FragmentContext;
import com.test.badoochallengetask.di.PerApplication;
import com.test.badoochallengetask.screens.transactions.TransactionsFragment;

import dagger.Component;


@PerApplication
@FragmentContext
@Component(dependencies = {AppComponent.class}, modules = {TransactionsModule.class})
public interface TransactionsComponent {
    void inject(TransactionsFragment fragment);
}
