package com.test.badoochallengetask.screens.transactions;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.test.badoochallengetask.base.presenter.BasePresenter;
import com.test.badoochallengetask.models.Product;
import com.test.badoochallengetask.models.ProductPrice;
import com.test.badoochallengetask.screens.transactions.model.TransactionManager;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

class TransactionPresenter extends BasePresenter<TransactionsFragment> {

    @Nullable
    private final Product mProduct;

    @Inject
    TransactionInteractor mInteractor;

    @Inject
    TransactionPresenter(@NonNull final TransactionsFragment view, @Nullable final Product product) {
        super(view);
        mProduct = product;
        if (mProduct != null) {
            view.setTitle(product.getSku());
        } else {
            view.showNoDataAvailable();
        }
    }

    void load() {
        final Disposable disposable = mInteractor.getRateManager()
                .flatMap(new Function<RateManager, SingleSource<TransactionManager>>() {
                    @Override
                    public SingleSource<TransactionManager> apply(@io.reactivex.annotations.NonNull final RateManager rateManager) throws Exception {
                        return getTransactionObservable(rateManager);
                    }
                })
                .cache()
                .doOnSubscribe(d -> publishState(RequestState.LOADING))
                .doOnSuccess(d -> publishState(RequestState.COMPLETE))
                .doOnError(d -> publishState(RequestState.ERROR))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .subscribe((transactionManager, throwable) -> {
                    if (throwable != null) {
                        getView().displayErrorMessage(throwable);
                        Timber.e(throwable);
                    } else {
                        getView().updateData(transactionManager);
                    }
                });
        addToDisposable(disposable);
    }

    @NonNull
    private Single<TransactionManager> getTransactionObservable(@NonNull final RateManager rateManager) {
        return Single.create(singleEmitter -> {
            if (mProduct == null) {
                singleEmitter.onError(new NullPointerException("Products are null"));
            } else {
                final TransactionManager transactionManager = new TransactionManager();
                for (final ProductPrice productPrice : mProduct.getTransactions()) {
                    TransactionManager.PriceItem priceItem = new TransactionManager.PriceItem(productPrice, rateManager.getConvertedProductPrice(productPrice));
                    transactionManager.addTransaction(priceItem);
                }
                singleEmitter.onSuccess(transactionManager);
            }
        });
    }
}
