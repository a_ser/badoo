package com.test.badoochallengetask.screens.base_list;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.badoochallengetask.R;
import com.test.badoochallengetask.base.presenter.IPresenter;
import com.test.badoochallengetask.base.view.BaseFragment;

public abstract class BaseListFragment<P extends IPresenter, D> extends BaseFragment<P> {
    private BaseListAdapter<D> mListAdapter;
    private TextView mSupportTextView;

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        injectComponents();
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_list, container, false);
        initViews(view);
        return view;
    }

    protected abstract void injectComponents();

    private void initViews(@NonNull final View view) {
        mSupportTextView = (TextView) view.findViewById(R.id.fragment_list_support_view);
        final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.fragment_list_recycler_view);
        mListAdapter = getAdapter();
        // TODO change for tablet
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(view.getContext(), getResources().getInteger(R.integer.grid_image_count));
        recyclerView.addItemDecoration(new MarginItemDecoration());
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(mListAdapter);
    }

    @NonNull
    protected abstract BaseListAdapter<D> getAdapter();

    @Override
    public void manageLoadingVisibility(final boolean isVisible) {
        showSupportText(getString(R.string.loading));
    }

    public void showNoDataAvailable() {
        mListAdapter.clear();
        showSupportText(getString(R.string.no_data_available));
    }

    public void updateData(@NonNull final D data) {
        mListAdapter.setData(data);
        mListAdapter.notifyDataSetChanged();
        mSupportTextView.setVisibility(View.GONE);
    }

    public void displayErrorMessage(@NonNull final Throwable throwable) {
        showSupportText(getString(R.string.error_happened_xs, throwable.getMessage()));
    }

    private void showSupportText(@NonNull String message) {
        mSupportTextView.setVisibility(View.VISIBLE);
        mSupportTextView.setText(message);
    }
}
