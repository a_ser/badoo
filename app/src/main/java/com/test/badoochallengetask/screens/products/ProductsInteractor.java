package com.test.badoochallengetask.screens.products;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.test.badoochallengetask.base.DataProvider;
import com.test.badoochallengetask.exceptions.IllegalProductFormat;
import com.test.badoochallengetask.models.Product;
import com.test.badoochallengetask.models.ProductManager;
import com.test.badoochallengetask.models.ProductRaw;
import com.test.badoochallengetask.utils.Constants;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;

class ProductsInteractor {
    private static final String TRANSACTIONS_FILE_NAME = "transactions.json";
    @Inject
    DataProvider mDataProvider;

    @Inject
    ProductsInteractor() {

    }

    @NonNull
    Single<List<Product>> loadList() {
        return Single.create((SingleOnSubscribe<List<Product>>) singleEmitter -> {
            try {
                final InputStream inputStream = mDataProvider.openAssets(TRANSACTIONS_FILE_NAME);
                Reader reader = new InputStreamReader(inputStream, Constants.UTF_8);
                final ProductRaw[] products = new Gson().fromJson(reader, ProductRaw[].class);
                final ProductManager productManager = new ProductManager();
                for (final ProductRaw product : products) {
                    if (product.getSku() == null || product.getAmount() == null || product.getCurrency() == null) {
                        singleEmitter.onError(new IllegalProductFormat("Product shouldn't have empty SKU, Amount and Currency"));
                        return;
                    }
                    productManager.add(product.getSku(), product.getAmount(), product.getCurrency());
                }
                singleEmitter.onSuccess(productManager.getProducts());
            } catch (Exception exception) {
                singleEmitter.onError(exception);
            }
        }).cache();
    }


}
