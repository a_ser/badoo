package com.test.badoochallengetask.screens.base_list;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;

public abstract class BaseListAdapter<D> extends RecyclerView.Adapter {
    @Nullable
    private D mData;

    public void setData(@Nullable D data) {
        mData = data;
    }

    public abstract void clear();

    @Nullable
    public D getData() {
        return mData;
    }

    @NonNull
    protected D getDataOrThrow() {
        if (mData == null) {
            throw new NullPointerException("It's impossible to interact with empty data");
        }
        return mData;
    }
}
