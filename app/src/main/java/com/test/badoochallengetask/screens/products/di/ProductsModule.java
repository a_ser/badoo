package com.test.badoochallengetask.screens.products.di;

import android.content.Context;
import android.support.annotation.NonNull;

import com.test.badoochallengetask.di.FragmentContext;
import com.test.badoochallengetask.screens.products.ProductsFragment;

import dagger.Module;
import dagger.Provides;


@Module
public class ProductsModule {
    @NonNull
    private final ProductsFragment mView;

    public ProductsModule(@NonNull final ProductsFragment view) {
        mView = view;
    }

    @Provides
    @NonNull
    ProductsFragment providesView() {
        return mView;
    }

    @Provides
    @FragmentContext
    Context providesContext() {
        return mView.getContext();
    }
}
