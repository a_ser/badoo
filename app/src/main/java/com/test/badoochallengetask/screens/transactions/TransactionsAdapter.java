package com.test.badoochallengetask.screens.transactions;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.badoochallengetask.R;
import com.test.badoochallengetask.models.ProductPrice;
import com.test.badoochallengetask.screens.base_list.BaseListAdapter;
import com.test.badoochallengetask.screens.transactions.model.TransactionManager;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

class TransactionsAdapter extends BaseListAdapter<TransactionManager> {
    private static final int HEADER_TYPE = 0;
    private static final int ITEMS_TYPE = 1;

    @Override
    public void clear() {
        setData(null);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        if (viewType == HEADER_TYPE) {
            return new HeaderViewHolder(parent);
        } else {
            return new ItemViewHolder(parent);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder parentHolder, final int position) {
        final Context context = parentHolder.itemView.getContext();
        if (position == 0) {
            HeaderViewHolder viewHolder = (HeaderViewHolder) parentHolder;
            final ProductPrice totalPrice = getDataOrThrow().getTotalPrice();
            if (totalPrice != null) {
                viewHolder.headerTextView.setText(context.getString(R.string.total_xs, getFormattedPrice(totalPrice)));
            }
        } else {
            final TransactionManager.PriceItem priceItem = getDataOrThrow().getTransactions().get(position - 1);
            ItemViewHolder viewHolder = (ItemViewHolder) parentHolder;
            viewHolder.currentPriceTextView.setText(getFormattedPrice(priceItem.getCurrentPrice()));
            viewHolder.convertedPriceTextView.setText(getFormattedPrice(priceItem.getConvertedPrice()));
        }
    }

    @Override
    public int getItemViewType(final int position) {
        if (position == 0) {
            return HEADER_TYPE;
        } else {
            return ITEMS_TYPE;
        }
    }

    @Override
    public int getItemCount() {
        // Header + items
        return getData() == null ? 0 : getData().getTransactions().size() + 1;
    }

    @NonNull
    private String getFormattedPrice(@NonNull final ProductPrice price) {
        final NumberFormat numberFormatter = NumberFormat.getCurrencyInstance(Locale.ENGLISH);
        numberFormatter.setCurrency(price.getCurrency());
        numberFormatter.setMinimumFractionDigits(2);
        return numberFormatter.format(price.getAmount().setScale(2, BigDecimal.ROUND_HALF_EVEN));
    }


    private static class HeaderViewHolder extends RecyclerView.ViewHolder {
        private TextView headerTextView;

        HeaderViewHolder(@NonNull final ViewGroup parent) {
            super(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_transaction_header, parent, false));
            headerTextView = (TextView) itemView;
        }
    }

    private static class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView currentPriceTextView;
        private TextView convertedPriceTextView;

        ItemViewHolder(@NonNull final ViewGroup parent) {
            super(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_transaction_item, parent, false));
            currentPriceTextView = (TextView) itemView.findViewById(R.id.view_holder_transaction_current_price);
            convertedPriceTextView = (TextView) itemView.findViewById(R.id.view_holder_transaction_converted_price);
        }
    }
}
