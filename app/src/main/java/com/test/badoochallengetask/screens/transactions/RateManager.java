package com.test.badoochallengetask.screens.transactions;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.Size;

import com.test.badoochallengetask.exceptions.IllegalPriceFormat;
import com.test.badoochallengetask.exceptions.IllegalRateFormat;
import com.test.badoochallengetask.exceptions.IllegalRateState;
import com.test.badoochallengetask.models.ProductPrice;
import com.test.badoochallengetask.utils.Constants;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Adds currency exchange rules and can convert from one currency to another.
 * Manger is using algorithm Dijkstra to finding shortest path.
 * If two or more paths possible - one with the cheapest rate will be returned
 */
class RateManager {
    @NonNull
    private Graph mGraph = new Graph();

    public void add(@NonNull final String from, @NonNull final String rate, @NonNull final String to) {
        mGraph.add(from, to, Float.parseFloat(rate));
    }

    @NonNull
    ProductPrice getConvertedProductPrice(@NonNull final ProductPrice currentPrice) throws IllegalPriceFormat, IllegalRateFormat, IllegalRateState {
        return getConvertedProductPrice(currentPrice, Constants.DEFAULT_CURRENCY);
    }

    @NonNull
    ProductPrice getConvertedProductPrice(@NonNull final ProductPrice currentPrice, @NonNull String convertTo) throws IllegalRateFormat, IllegalPriceFormat, IllegalRateState {
        final float rate = mGraph.getRate(currentPrice.getCurrency().getCurrencyCode(), convertTo);
        return new ProductPrice(currentPrice.getAmount().multiply(BigDecimal.valueOf(rate)), convertTo);
    }

    private static class Graph {
        @NonNull
        private Set<Node> mNodes = new HashSet<>();

        public void add(@NonNull final String from, @NonNull final String to, final float rate) {
            Node nodeFrom = null;
            Node nodeTo = null;
            for (final Node node : mNodes) {
                if (node.mName.equals(from)) {
                    nodeFrom = node;
                } else if (node.mName.equals(to)) {
                    nodeTo = node;
                }
            }

            if (nodeFrom == null) {
                nodeFrom = new Node(from);
            }

            if (nodeTo == null) {
                nodeTo = new Node(to);
            }

            nodeFrom.add(nodeTo, rate);
            mNodes.add(nodeFrom);
            mNodes.add(nodeTo);
        }

        /**
         * Return 1.0 when {@param from} equals {@param to}.
         * Otherwise, return best rate of exchanging
         *
         * @param from currency we have
         * @param to   currency we want to get after exchanging
         * @return 1.0 for the same currency or best rate
         * @throws IllegalRateState when {@param from} currency doesn't exist in graph
         */
        float getRate(@NonNull final String from, @NonNull final String to) throws IllegalRateState {
            Node node = null;
            for (final Node nodeElement : mNodes) {
                if (nodeElement.mName.equals(from)) {
                    node = nodeElement;
                    break;
                }
            }
            if (node == null) {
                throw new IllegalRateState(String.format("No such currency %s", from));
            }
            return node.findRate(from, to);
        }
    }

    private static class Node {
        private static final int VERTEX_INDEX = 0;
        private static final int RATE_INDEX = 1;
        private String mName;
        private Map<Node, Float> mNodes = new HashMap<>();
        private boolean isVisited = false;

        Node(@NonNull final String name) {
            mName = name;
        }

        public void add(Node node, float rate) {
            mNodes.put(node, rate);
        }

        /**
         * Find result rate for exchanging currency from {@param currencyFrom} to {@param currencyTo}
         *
         * @param currencyFrom which we want to convert
         * @param currencyTo   desired currency
         * @return resulting rate of conversion
         * @throws IllegalRateState when path from one currency to another doesn't exist in graph
         */
        private float findRate(@NonNull final String currencyFrom, @NonNull final String currencyTo) throws IllegalRateState {
            resetState();
            final float[] vertexAndRateForPath = find(currencyTo);
            if (vertexAndRateForPath == null) {
                throw new IllegalRateState(String.format("There is no possible way to exchange %s into %s", currencyFrom, currencyTo));
            }
            return vertexAndRateForPath[RATE_INDEX];
        }

        /**
         * Reset state of visited Nodes
         */
        private void resetState() {
            for (Node node : mNodes.keySet()) {
                node.isVisited = false;
            }
        }

        /**
         * Iterate through vertex and find shortest path.
         * If exists two or more equal paths the one with lowest rate will be returned.
         *
         * @param currencyTo code of currency we are building path to
         * @return array with size of 2.
         * {@link #VERTEX_INDEX} is responsible for number of vertex
         * from current note to desired.
         * {@link #RATE_INDEX} of array is responsible for rate of exchange.
         */
        @Nullable
        @Size(2)
        private float[] find(@NonNull final String currencyTo) {
            if (mName.equals(currencyTo)) {
                // Allow others reach this vertex
                isVisited = false;
                return new float[]{0, 1};
            }
            if (!mNodes.isEmpty()) {
                float[] result = null;
                for (final Map.Entry<Node, Float> nodeFloatEntry : mNodes.entrySet()) {
                    if (!nodeFloatEntry.getKey().isVisited) {
                        nodeFloatEntry.getKey().isVisited = true;
                        final float[] integerIntegerPair = nodeFloatEntry.getKey().find(currencyTo);
                        if (integerIntegerPair != null) {
                            final float vertexes = integerIntegerPair[VERTEX_INDEX] + 1;
                            final float rate = integerIntegerPair[RATE_INDEX] * nodeFloatEntry.getValue();
                            if (result == null) {
                                result = new float[]{vertexes, rate};
                            } else if (vertexes < result[VERTEX_INDEX] || vertexes == result[VERTEX_INDEX] && rate < result[RATE_INDEX]) {
                                result = new float[]{vertexes, rate};
                            }

                        }
                    }
                }
                return result;
            }
            return null;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final Node node = (Node) o;
            return mName.equals(node.mName);

        }

        @Override
        public int hashCode() {
            return mName.hashCode();
        }
    }
}
