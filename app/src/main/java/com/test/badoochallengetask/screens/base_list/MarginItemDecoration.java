package com.test.badoochallengetask.screens.base_list;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

class MarginItemDecoration extends RecyclerView.ItemDecoration {
    @Override
    public void getItemOffsets(final Rect outRect, final View view, final RecyclerView parent, final RecyclerView.State state) {
        final int space = 20;
        outRect.bottom = 2 * space;
    }
}
