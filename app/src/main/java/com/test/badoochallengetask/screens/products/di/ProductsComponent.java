package com.test.badoochallengetask.screens.products.di;


import com.test.badoochallengetask.di.AppComponent;
import com.test.badoochallengetask.di.FragmentContext;
import com.test.badoochallengetask.di.PerApplication;
import com.test.badoochallengetask.screens.products.ProductsFragment;

import dagger.Component;


@PerApplication
@FragmentContext
@Component(dependencies = AppComponent.class, modules = { ProductsModule.class })
public interface ProductsComponent {
    void inject(ProductsFragment view);
}
