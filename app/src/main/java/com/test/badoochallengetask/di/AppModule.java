package com.test.badoochallengetask.di;

import android.content.Context;
import android.support.annotation.NonNull;

import com.test.badoochallengetask.MainApplication;
import com.test.badoochallengetask.base.DataProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class AppModule {
    private final MainApplication mApplication;

    public AppModule(@NonNull final MainApplication application) {
        mApplication = application;
    }

    @Singleton
    @Provides
    MainApplication provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication.getApplicationContext();
    }

    @Singleton
    @Provides
    DataProvider provideDataProvider(@ApplicationContext @NonNull final Context context) {
        return new DataProvider(context);
    }
}
