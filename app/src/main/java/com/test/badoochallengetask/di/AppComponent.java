package com.test.badoochallengetask.di;


import com.test.badoochallengetask.MainApplication;
import com.test.badoochallengetask.base.DataProvider;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@ApplicationContext
@Component(modules = { AppModule.class })
public interface AppComponent {
    void inject(MainApplication application);

    DataProvider getDataProvider();

}
