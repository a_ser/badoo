package com.test.badoochallengetask.exceptions;

/**
 * Thrown when parsing of the price and its currency fail
 */
public class IllegalPriceFormat extends Exception {
    public IllegalPriceFormat(final Throwable e) {
        super(e);
    }
}
