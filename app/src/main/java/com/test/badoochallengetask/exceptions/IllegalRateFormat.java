package com.test.badoochallengetask.exceptions;

/**
 * Thrown when {@link com.test.badoochallengetask.models.Rate} doesn't have required parameters
 */
public class IllegalRateFormat extends Exception {
    public IllegalRateFormat(final String error) {
        super(error);
    }
}
