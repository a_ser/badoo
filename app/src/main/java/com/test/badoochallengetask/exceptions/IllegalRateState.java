package com.test.badoochallengetask.exceptions;

/**
 * Thrown when manipulations with {@link com.test.badoochallengetask.models.Rate} have failed
 */

public class IllegalRateState extends Exception {
    public IllegalRateState(final String error) {
        super(error);
    }
}
