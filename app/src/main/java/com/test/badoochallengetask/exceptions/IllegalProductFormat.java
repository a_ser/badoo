package com.test.badoochallengetask.exceptions;

/**
 * Thrown when {@link com.test.badoochallengetask.models.Product} doesn't have all
 * required parameters.
 */
public class IllegalProductFormat extends IllegalArgumentException {

    public IllegalProductFormat(final String error) {
        super(error);
    }
}
