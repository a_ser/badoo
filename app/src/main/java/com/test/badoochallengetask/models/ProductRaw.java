package com.test.badoochallengetask.models;

import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
@SuppressWarnings("unused")
public class ProductRaw {

    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("currency")
    @Expose
    private String currency;

    @Nullable
    public String getAmount() {
        return amount;
    }

    @Nullable
    public String getSku() {
        return sku;
    }


    @Nullable
    public String getCurrency() {
        return currency;
    }

}
