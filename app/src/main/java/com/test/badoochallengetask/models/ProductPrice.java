package com.test.badoochallengetask.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.test.badoochallengetask.exceptions.IllegalPriceFormat;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Currency;

public class ProductPrice implements Parcelable {
    public static final Creator<ProductPrice> CREATOR = new Creator<ProductPrice>() {
        @Override
        public ProductPrice createFromParcel(Parcel in) {
            return new ProductPrice(in);
        }

        @Override
        public ProductPrice[] newArray(int size) {
            return new ProductPrice[size];
        }
    };
    @NonNull
    private Currency mCurrency;
    @NonNull
    private BigDecimal mAmount;

    public ProductPrice(@NonNull final String amount, @NonNull final String currency) throws IllegalPriceFormat {
        try {
            mAmount = new BigDecimal(amount, MathContext.DECIMAL32);
            mCurrency = Currency.getInstance(currency);
        } catch (IllegalArgumentException | ArithmeticException e) {
            throw new IllegalPriceFormat(e);
        }
    }

    public ProductPrice(@NonNull final BigDecimal amount, @NonNull final String currency) throws IllegalPriceFormat {
        mAmount = amount;
        try {
            mCurrency = Currency.getInstance(currency);
        } catch (IllegalArgumentException e) {
            throw new IllegalPriceFormat(e);
        }
    }

    private ProductPrice(@NonNull Parcel in) {
        mAmount = (BigDecimal) in.readSerializable();
        mCurrency = (Currency) in.readSerializable();
    }

    public ProductPrice(@NonNull final BigDecimal amount, @NonNull final Currency currency) {
        mAmount = amount;
        mCurrency = currency;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeSerializable(mAmount);
        dest.writeSerializable(mCurrency);
    }

    public void setAmount(@NonNull final BigDecimal amount) {
        mAmount = amount;
    }

    @NonNull
    public Currency getCurrency() {
        return mCurrency;
    }

    @NonNull
    public BigDecimal getAmount() {
        return mAmount;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final ProductPrice that = (ProductPrice) o;
        return mCurrency.equals(that.mCurrency) && mAmount.equals(that.mAmount);
    }

    @Override
    public int hashCode() {
        int result = mCurrency.hashCode();
        result = 31 * result + mAmount.hashCode();
        return result;
    }
}
