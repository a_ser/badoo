package com.test.badoochallengetask.models;

import android.support.annotation.NonNull;

import com.test.badoochallengetask.exceptions.IllegalPriceFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductManager {
    @NonNull
    private final Map<String, Product> mProductsMap;

    public ProductManager() {
        mProductsMap = new HashMap<>();
    }

    public void add(@NonNull final String sku, @NonNull final String amount, @NonNull final String currency) throws IllegalPriceFormat {
        Product product = mProductsMap.get(sku);
        if (product == null) {
            product = new Product(sku);
            mProductsMap.put(sku, product);
        }
        product.addTransaction(amount, currency);
    }

    @NonNull
    public List<Product> getProducts() {
        return new ArrayList<>(mProductsMap.values());
    }
}
