package com.test.badoochallengetask.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.test.badoochallengetask.exceptions.IllegalPriceFormat;

import java.util.ArrayList;
import java.util.List;

public class Product implements Parcelable {
    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
    @NonNull
    private final String mSku;
    @NonNull
    private final List<ProductPrice> mTransactions;

    Product(@NonNull final String sku) {
        mSku = sku;
        mTransactions = new ArrayList<>();
    }

    protected Product(Parcel in) {
        mSku = in.readString();
        mTransactions = in.createTypedArrayList(ProductPrice.CREATOR);
    }

    void addTransaction(@NonNull final String amount, @NonNull final String currency) throws IllegalPriceFormat {
        mTransactions.add(new ProductPrice(amount, currency));
    }

    @NonNull
    public String getSku() {
        return mSku;
    }

    @NonNull
    public List<ProductPrice> getTransactions() {
        return mTransactions;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeString(mSku);
        dest.writeTypedList(mTransactions);
    }
}
