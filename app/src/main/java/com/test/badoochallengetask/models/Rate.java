package com.test.badoochallengetask.models;

import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Rate {

    @SerializedName("from")
    @Expose
    private String from;
    @SerializedName("rate")
    @Expose
    private String rate;
    @SerializedName("to")
    @Expose
    private String to;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    @Nullable
    public String getRate() {
        return rate;
    }


    @Nullable
    public String getTo() {
        return to;
    }
}