package com.test.badoochallengetask;

import android.app.Application;

import com.test.badoochallengetask.di.AppComponent;
import com.test.badoochallengetask.di.AppModule;
import com.test.badoochallengetask.di.DaggerAppComponent;

import timber.log.Timber;

public class MainApplication extends Application {
    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    public AppComponent getAppComponent() {
        if (mAppComponent == null) {
            mAppComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
        }
        return mAppComponent;
    }
}
