package com.test.badoochallengetask.utils;

public interface Constants {
    String UTF_8 = "UTF-8";
    String DEFAULT_CURRENCY = "GBP";
}
