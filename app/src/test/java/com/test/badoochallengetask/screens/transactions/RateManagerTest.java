package com.test.badoochallengetask.screens.transactions;

import com.test.badoochallengetask.exceptions.IllegalPriceFormat;
import com.test.badoochallengetask.exceptions.IllegalRateFormat;
import com.test.badoochallengetask.exceptions.IllegalRateState;
import com.test.badoochallengetask.models.ProductPrice;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RateManagerTest {
    @Test
    public void testGetConvertedProductPriceShouldReturnSameDataWhenConvertFromUsdToUsd() {
        final RateManager rateManager = new RateManager();
        rateManager.add("CAD", "0.92", "USD");
        rateManager.add("USD", "1.09", "CAD");
        rateManager.add("USD", "0.77", "GBP");
        rateManager.add("GBP", "1.3", "USD");
        rateManager.add("GBP", "0.83", "AUD");
        rateManager.add("AUD", "0.77", "GBP");
        boolean isExceptionHappened = false;
        try {
            final ProductPrice givenPrice = new ProductPrice("1.22000", "USD");
            final String convertTo = "USD";
            final ProductPrice price = rateManager.getConvertedProductPrice(givenPrice, convertTo);
            assertEquals(givenPrice.getCurrency().getCurrencyCode(), "USD");
            assertEqualsBigDecimals(price.getAmount(), BigDecimal.valueOf(1.22));
        } catch (IllegalRateFormat | IllegalPriceFormat | IllegalRateState e) {
            e.printStackTrace();
            isExceptionHappened = true;
        }
        assertFalse(isExceptionHappened);
    }

    @Test
    public void testGetConvertedProductPriceShouldReturnErrorWhenConvertPathDoesntExist() {
        final RateManager rateManager = new RateManager();
        rateManager.add("CAD", "0.92", "USD");
        rateManager.add("USD", "1.09", "CAD");
        rateManager.add("USD", "0.77", "GBP");
        rateManager.add("GBP", "0.83", "AUD");
        rateManager.add("AUD", "0.77", "GBP");
        boolean isExceptionHappened = false;
        try {
            final ProductPrice givenPrice = new ProductPrice("1.22", "AUD");
            final String convertTo = "CAD";
            rateManager.getConvertedProductPrice(givenPrice, convertTo);
            isExceptionHappened = false;
        } catch (IllegalRateFormat | IllegalPriceFormat illegalRateFormat) {
            illegalRateFormat.printStackTrace();
        } catch (IllegalRateState illegalRateState) {
            isExceptionHappened = true;
        }
        assertTrue(isExceptionHappened);
    }

    @Test
    public void testGetConvertedProductPriceShouldReturnShortestPathForTheSameSumOfRate() {
        final RateManager rateManager = new RateManager();
        rateManager.add("CAD", "0.92", "USD");
        rateManager.add("USD", "0.77", "GBP");
        rateManager.add("CAD", "0.77", "GBP");
        boolean isExceptionHappened = false;
        try {
            final ProductPrice givenPrice = new ProductPrice("1.22", "CAD");
            final String convertTo = "GBP";
            final ProductPrice convertedProductPrice = rateManager.getConvertedProductPrice(givenPrice, convertTo);

            assertEquals(convertedProductPrice.getCurrency().getCurrencyCode(), "GBP");
            assertEqualsBigDecimals(convertedProductPrice.getAmount(), BigDecimal.valueOf(0.9394));
        } catch (IllegalRateFormat | IllegalPriceFormat | IllegalRateState e) {
            e.printStackTrace();
            isExceptionHappened = true;
        }
        assertFalse(isExceptionHappened);
    }

    @Test
    public void testGetConvertedProductPriceShouldReturnLowestRateForEqualPathLength() {
        final RateManager rateManager = new RateManager();
        rateManager.add("CAD", "0.92", "USD");
        rateManager.add("USD", "0.77", "GBP");
        rateManager.add("CAD", "0.92", "AUD");
        rateManager.add("AUD", "0.76", "GBP");
        boolean isExceptionHappened = false;
        try {
            final ProductPrice givenPrice = new ProductPrice("1.22", "CAD");
            final String convertTo = "GBP";
            final ProductPrice convertedProductPrice = rateManager.getConvertedProductPrice(givenPrice, convertTo);

            assertEquals(convertedProductPrice.getCurrency().getCurrencyCode(), "GBP");
            assertEqualsBigDecimals(convertedProductPrice.getAmount(), BigDecimal.valueOf(0.853024));
        } catch (IllegalRateFormat | IllegalPriceFormat | IllegalRateState e) {
            e.printStackTrace();
            isExceptionHappened = true;
        }
        assertFalse(isExceptionHappened);
    }

    private void assertEqualsBigDecimals(final BigDecimal bigDecimal, final BigDecimal bigDecimal1) {
        assertEquals(bigDecimal.setScale(4, BigDecimal.ROUND_HALF_EVEN), bigDecimal1.setScale(4, BigDecimal.ROUND_HALF_EVEN));
    }

}