package com.test.badoochallengetask.screens.transactions.model;

import com.test.badoochallengetask.exceptions.IllegalPriceFormat;
import com.test.badoochallengetask.models.ProductPrice;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class TransactionManagerTest {
    @Test
    public void testAddTransactionShouldAddAllTransaction() {
        final TransactionManager transactionManager = new TransactionManager();
        boolean isExceptionHappened = false;
        try {
            final TransactionManager.PriceItem transaction1 = new TransactionManager.PriceItem(new ProductPrice("1.22", "USD"), new ProductPrice("1.22", "GBP"));
            final ProductPrice convertedPriceTransaction2 = new ProductPrice("5.52", "GBP");
            final TransactionManager.PriceItem transaction2 = new TransactionManager.PriceItem(new ProductPrice("12.22", "EUR"), convertedPriceTransaction2);
            final TransactionManager.PriceItem transaction3 = new TransactionManager.PriceItem(new ProductPrice("6.2", "CAD"), new ProductPrice("5.12", "GBP"));

            transactionManager.addTransaction(transaction1);
            transactionManager.addTransaction(transaction2);
            transactionManager.addTransaction(transaction3);

            final List<TransactionManager.PriceItem> transactions = transactionManager.getTransactions();
            assertNotNull(transactions);
            assertEquals(transactions.size(), 3);
            assertEquals(transactions.get(1).getConvertedPrice(), convertedPriceTransaction2);
        } catch (IllegalPriceFormat illegalPriceFormat) {
            isExceptionHappened = true;
        }
        assertFalse(isExceptionHappened);
    }

    @Test
    public void testAddTransactionShouldSumUpTotalCount() {
        final TransactionManager transactionManager = new TransactionManager();
        boolean isExceptionHappened = false;
        try {
            final TransactionManager.PriceItem transaction1 = new TransactionManager.PriceItem(new ProductPrice("1.22", "USD"), new ProductPrice("1.22", "GBP"));
            final TransactionManager.PriceItem transaction2 = new TransactionManager.PriceItem(new ProductPrice("12.22", "EUR"), new ProductPrice("5.52", "GBP"));
            final TransactionManager.PriceItem transaction3 = new TransactionManager.PriceItem(new ProductPrice("6.2", "CAD"), new ProductPrice("5.12", "GBP"));

            transactionManager.addTransaction(transaction1);
            transactionManager.addTransaction(transaction2);
            transactionManager.addTransaction(transaction3);

            final ProductPrice totalPrice = transactionManager.getTotalPrice();
            assertNotNull(totalPrice);
            assertEquals(totalPrice.getCurrency(), Currency.getInstance("GBP"));
            assertEquals(totalPrice.getAmount(), BigDecimal.valueOf(11.86));
        } catch (IllegalPriceFormat illegalPriceFormat) {
            isExceptionHappened = true;
        }
        assertFalse(isExceptionHappened);
    }

    @Test
    public void testAddTransactionShouldFailWhenAddTransactionsWithDifferentCurrencies() {
        final TransactionManager transactionManager = new TransactionManager();
        boolean isExceptionHappened = false;
        try {
            final TransactionManager.PriceItem transaction1 = new TransactionManager.PriceItem(new ProductPrice("1.22", "USD"), new ProductPrice("1.22", "GBP"));
            final TransactionManager.PriceItem transaction2 = new TransactionManager.PriceItem(new ProductPrice("12.22", "EUR"), new ProductPrice("5.52", "GBP"));
            final TransactionManager.PriceItem transaction3 = new TransactionManager.PriceItem(new ProductPrice("6.2", "CAD"), new ProductPrice("5.12", "EUR"));

            transactionManager.addTransaction(transaction1);
            transactionManager.addTransaction(transaction2);
            transactionManager.addTransaction(transaction3);
        } catch (IllegalPriceFormat ignored) {
        } catch (IllegalStateException e) {
            isExceptionHappened = true;
        }
        assertTrue(isExceptionHappened);
    }


}